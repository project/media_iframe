<?php

declare(strict_types=1);

namespace Drupal\media_iframe\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;

/**
 * Iframe entity media source.
 *
 * @MediaSource(
 *   id = \Drupal\media_iframe\Plugin\media\Source\InlineFrame::PLUGIN_ID,
 *   label = @Translation("Inline frame"),
 *   allowed_field_types = {"iframe"},
 *   default_thumbnail_filename = "iframe.png",
 *   description = @Translation("Provides inline frame media type."),
 *   forms = {
 *     "media_library_add" = \Drupal\media_iframe\Form\MediaLibrary\MediaIframeAddForm::class,
 *   },
 * )
 */
class InlineFrame extends MediaSourceBase {

  public const PLUGIN_ID = 'inline_frame';

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes(): array {
    return [];
  }

  public function createSourceField(MediaTypeInterface $type): FieldConfigInterface {
    return parent::createSourceField($type)->set('label', 'Inline Frame URL');
  }

  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display): void {
    $fieldName = $this->getSourceFieldDefinition($type)?->getName() ?? throw new \Exception('Media field was deleted');
    $display->setComponent($fieldName, [
      'type' => 'iframe_default',
      'label' => 'visually_hidden',
    ]);
  }

}
