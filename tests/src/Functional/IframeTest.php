<?php

declare(strict_types=1);

namespace Drupal\Tests\media_iframe\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\filter\Entity\FilterFormat;
use Drupal\media\Entity\Media;
use Drupal\media\Entity\MediaType;
use Drupal\media_iframe\Plugin\media\Source\InlineFrame;

/**
 * Tests for iframe.
 *
 * @group media_iframe
 */
final class IframeTest extends BrowserTestBase {

  protected $profile = 'standard';

  protected $defaultTheme = 'stark';

  protected static $modules = [
    'media_iframe',
  ];

  protected function setUp(): void {
    parent::setUp();
    // Create media type.
    $media_type = MediaType::create([
      'id' => 'inline_frame',
      'label' => 'iFrame',
      'description' => 'Provides Iframe media support.',
      'source' => InlineFrame::PLUGIN_ID,
    ]);
    $media_type->save();
    // Create the source field.
    $source_field = $media_type->getSource()->createSourceField($media_type);
    /** @var \Drupal\field\Entity\FieldStorageConfig $fieldStorage */
    $fieldStorage = $source_field->getFieldStorageDefinition();
    $fieldStorage->save();
    $source_field->save();
    $media_type
      ->set('source_configuration', [
        'source_field' => $source_field->getName(),
      ])
      ->save();
    /** @var \Drupal\filter\FilterFormatInterface $filter_format */
    $filter_format = FilterFormat::load('basic_html');
    // Enable media embed filter.
    $filter_format->setFilterConfig('media_embed', [
      'status' => TRUE,
      'settings' => [
        'default_view_mode' => 'default',
        'allowed_media_types' => [
          'inline_frame' => 'inline_frame',
        ],
        'allowed_view_modes' => [],
      ],
    ])->save();
    $config = $filter_format->filters('filter_html')
      ->getConfiguration();
    // Added drupal-media tags and attributes.
    $allowed_html = " <drupal-media data-entity-type data-entity-uuid data-view-mode>";
    $filter_format->setFilterConfig('filter_html', [
      'status' => TRUE,
      'settings' => [
        'allowed_html' => $config['settings']['allowed_html'] . $allowed_html,
      ],
    ])->save();
    // Update the view mode.
    \Drupal::service('entity_display.repository')
      ->getViewDisplay('media', 'inline_frame')
      ->setComponent('field_media_inline_frame', [
        'type' => 'iframe_default',
        'label' => 'visually_hidden',
      ])
      ->save();
  }

  /**
   * Test embedding a iframe into a WYSIWYG field with entity_embed.
   */
  public function testIframeEmbed(): void {
    // Please note viewing this in iframe using web browser doesn't actually
    // work. We're simply testing things here. If you want to test something
    // you can use "/" to load the homepage.
    $url = 'https://www.drupal.org/';
    $media = Media::create([
      'bundle' => 'inline_frame',
      'status' => TRUE,
      'name' => 'iframe',
      'field_media_inline_frame' => [
        'title' => 'Iframe title',
        'class' => 'iframe-class',
        'height' => '768',
        'width' => '1024',
        'url' => $url,
      ],
    ]);
    $media->save();
    $uuid = $media->uuid();
    $body = 'Abc <drupal-media data-entity-type="media" data-entity-uuid="' . $uuid . '"></drupal-media> 123';
    $content = $this->drupalCreateNode([
      'title' => 'Test content for Inline frame',
      'type' => 'page',
      'status' => '1',
      'body' => [
        'value' => $body,
        'format' => 'basic_html',
      ],
    ]);
    $this->drupalGet($content->toUrl());

    // Assert something basic to make sure the page loads correctly.
    $this->assertSession()->elementTextContains('css', 'h1', (string) $content->label());
    $this->assertSession()->pageTextContains('Abc');
    $this->assertSession()->pageTextContains('123');

    // Iframe and URL.
    $iframe = $this->assertSession()->elementExists('css', 'iframe');
    static::assertEquals('Iframe title', $iframe->getAttribute('title'));

    // Fix after https://www.drupal.org/project/iframe/issues/3390012
    if ((new \DateTimeImmutable()) > (new \DateTimeImmutable('30 Jan 2024'))) {
      static::assertEquals('iframe-class', $iframe->getAttribute('class'));
    }

    static::assertEquals('768', $iframe->getAttribute('height'));
    static::assertEquals('1024', $iframe->getAttribute('width'));
    static::assertEquals($url, $iframe->getAttribute('src'));
  }

}
